import Web3 from 'web3'

/*
 * 1. Check for injected web3 (mist/metamask)
 * 2. If metamask/mist create a new web3 instance and pass on result
 * 3. Get networkId - Now we can check the user is connected to the right network to use our dApp
 * 4. Get user account from metamask
 * 5. Get user balance
 */

let getWeb3 = new Promise(function (resolve, reject) {
    // Check for injected web3 (mist/metamask)
    // var web3js = window.web3
    window.addEventListener("load", async () => {
      // Modern dapp browsers...
      if (window.ethereum) {
        const web3 = new Web3(window.ethereum);
        console.log(web3, 11123)
        try {
          // Request account access if needed
          await window.ethereum.enable();
          // Acccounts now exposed
          // resolve(web3);
          resolve({

            // injectedWeb3: web3.isConnected(),
            web3() {
              return web3
            }
          })
        } catch (error) {
          reject(error);
        }
      }
      // Legacy dapp browsers...
      else if (window.web3) {
        // Use Mist/MetaMask's provider.
        const web3 = window.web3;
        console.log("Injected web3 detected.");
        resolve(web3);
      }
      // Fallback to localhost; use dev console port by default...
      else {
        const provider = new Web3.providers.HttpProvider(
          "http://127.0.0.1:9545"
        );
        const web3 = new Web3(provider);
        console.log("No web3 instance injected, using Local web3.");
        resolve(web3);
      }
    });

    // console.log(web3js)
    // if (typeof web3js !== 'undefined') {
    //   var web3 = new Web3(web3js.currentProvider)
    //   // console.log(web3,11123)
    //   resolve({

    //     // injectedWeb3: web3.isConnected(),
    //     web3() {
    //       return web3
    //     }
    //   })
    // } else {
    //   // web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545')) //GANACHE FALLBACK
    //   reject(new Error('Unable to connect to Metamask'))
    // }
  })
  .then(result => {
    return new Promise(function (resolve, reject) {
      // Retrieve network ID
      result.web3().eth.net.getId((err, networkId) => {
        if (err) {
          // If we can't find a networkId keep result the same and reject the promise
          reject(new Error('Unable to retrieve network ID'))
        } else {
          // Assign the networkId property to our result and resolve promise
          result = Object.assign({}, result, {
            networkId
          })
          resolve(result)
        }
      })
    })
  })
  .then(result => {
    return new Promise(function (resolve, reject) {
      // Retrieve coinbase
      result.web3().eth.getCoinbase((err, coinbase) => {
        if (err) {
          reject(new Error('Unable to retrieve coinbase'))
        } else {
          result = Object.assign({}, result, {
            coinbase
          })
          resolve(result)
        }
      })
    })
  })
  .then(result => {
    return new Promise(function (resolve, reject) {
      // Retrieve balance for coinbase
      result.web3().eth.getBalance(result.coinbase, (err, balance) => {
        if (err) {
          reject(new Error('Unable to retrieve balance for address: ' + result.coinbase))
        } else {
          result = Object.assign({}, result, {
            balance
          })
          resolve(result)
        }
      })
    })
  })

export default getWeb3