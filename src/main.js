import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import router from './router/index'
import { store } from './store/'
Vue.use(Vuex)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store,
  router
}).$mount('#app')
